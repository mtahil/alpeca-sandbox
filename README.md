# Pre-requisits 

1. A personal account on [alpaca.markets](https://alpaca.markets) trading platform.
2. Generated API and Secret keys
3. Account on TradingView 
4. Connect TradingView with Alpeca by following [these instructions](https://alpaca.markets/docs/alpaca-works-with/tradingview/)

