FROM alpacahq/pylivetrader

ENV MODULE trump_tweets

RUN mkdir /config && mkdir /module && pip install redis

# RUN cp $MODULE/* /module

# WORKDIR /module
# RUN pip install -r requirements.txt

# WORKDIR /config
# RUN touch config.yaml && \
# 	echo "key_id: $API_KEY" > ./config.yaml && \
# 	echo "secret: $SECRET_KEY" >> ./config.yaml && \
# 	echo "base_url: $ENDPOINT" >> ./config.yaml && \
# 	echo "use_polygon: false" >> ./config.yaml

WORKDIR /tmp

CMD pylivetrader run -f /module/$MODULE.py --backend-config /config/config.yaml